package edu.uri.uribustracker;
import java.util.Observable;
import java.lang.Runnable;
import java.util.ArrayList;
import java.net.*;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;
import org.json.*;
import java.util.concurrent.*;
import android.util.Log;


/**
 * Created by ray on 4/17/16.
 */
public class CoordinatesFetcher extends Observable {
    private static final String TAG = CoordinatesFetcher.class.getSimpleName();
    private ArrayList<BusData> mBusData;
    private ExecutorService mPool;
    private Timer mTimer;
    private Future mFuture;
    private boolean running;
    private boolean errorFlag = false;
    public boolean hadError() {
        return errorFlag;
    }
    private Thread.UncaughtExceptionHandler mUncaughtExceptionHandler;
    public CoordinatesFetcher(int interval) {
        super();
        mPool = Executors.newFixedThreadPool(2);
        final CoordinatesFetcher self = this;
        mTimer = new Timer(true);
        mFuture = fetch();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (running) return;
                running = true;
                try {
                    mBusData = (ArrayList<BusData>) mFuture.get();
                    setChanged();
                    notifyObservers(mBusData);
                    clearChanged();
                    errorFlag = false;
                } catch (Exception e) {
                    errorFlag = true;
                }
                mFuture = fetch();
                running = false;
            }
        }, 0, interval);
    }
    public Future fetch() {
        return mPool.submit(new Callable() {
            public ArrayList<BusData> call() throws Exception {
                    String tmp;
                    URL url = new URL("http://bus.apps.uri.edu/paraVehicle.php");
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    StringBuilder jsonBuilder = new StringBuilder();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    while ((tmp = reader.readLine()) != null) {
                        jsonBuilder.append(tmp);
                    }
                    JSONArray array = new JSONArray(jsonBuilder.toString());
                    mBusData = new ArrayList<BusData>();
                    BusData ref;
                    JSONObject bus;
                    for (int i = 0; i < array.length(); ++i) {
                        ref = new BusData();
                        bus = array.getJSONObject(i);
                        ref.latitude = bus.getDouble("Latitude");
                        ref.longitude = bus.getDouble("Longitude");
                        ref.time = bus.getString("Time");
                        ref.route = bus.getString("Route");
                        ref.vehicle = bus.getString("Vehicle");
                        mBusData.add(ref);
                    }
                return mBusData;
            }
        });

    }
}
