package edu.uri.uribustracker;

/**
 * Created by ray on 4/17/16.
 */
public class RouteSelector {
    public static MapActivity mMapActivity;
    public static void setMapActivity(MapActivity a) {
        mMapActivity = a;
    }
    static int selection = 0;
    static int RED_ROUTE = 0x01;
    static int BLUE_ROUTE = 0x02;
    static void unselect() {
        selection = 0;
        if (mMapActivity.mBuses != null) mMapActivity.update(mMapActivity.mCoordinatesFetcher, mMapActivity.mBuses);
    }
    static void unselect(int m) {
        selection &= ~m;
        if (mMapActivity.mBuses != null) mMapActivity.update(mMapActivity.mCoordinatesFetcher, mMapActivity.mBuses);
    }
    static void select(int m) {
        selection |= m;
        if (mMapActivity.mBuses != null) mMapActivity.update(mMapActivity.mCoordinatesFetcher, mMapActivity.mBuses);
    }
    private static int routeToEnum(Route r) {
        if (r == Route.getBlueRoute()) {
            return BLUE_ROUTE;
        } else if (r == Route.getHillClimberRoute()) {
            return RED_ROUTE;
        }
        return 0;
    }
    static boolean isSelected(Route r) {
        if ((routeToEnum(r) & selection) != 0) {
            return true;
        }
        return false;
    }
}
